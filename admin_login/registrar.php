﻿<?php require_once 'config.php'; ?>
<?php 
    if(!empty($_POST)){
        try {
            $user_obj = new Cl_User();
            $data = $user_obj->registration( $_POST );
            if($data)$success = USER_REGISTRATION_SUCCESS;
        } catch (Exception $e) {
            $error = $e->getMessage();
        }
    }
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Registro Nuevo Usuario</title>
    <!-- Favicon-->
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="css/style.css" rel="stylesheet">
</head>

<body class="signup-page">
    <div class="signup-box">
        <div class="">
            <center><img src="images/logo_blanco.png" class="img-responsive text-center"></center>  
        </div>
        <div class="card">
            <?php require_once 'templates/message.php';?>
            <div class="body">
                
                <form form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" class="form-register" role="form" id="register-form">
                    <div class="msg">Registrar Nuevo Usuario</div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                        <div class="form-line">
                           <input name="name" id="name" type="text" class="form-control" placeholder="Nombres" required autofocus>
                           <span class="help-block"></span> 
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">email</i>
                        </span>
                        <div class="form-line">
                            <input name="email" id="email" type="email" class="form-control" placeholder="Correo electrónico" required>
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            <input name="password" id="password" type="password" class="form-control" placeholder="Contraseña" required>
                            <span class="help-block"></span> 
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            <input name="confirm_password" id="confirm_password" type="password" class="form-control" placeholder="Confirmar Contraseña">
                            <span class="help-block"></span>
                        </div>
                    </div>

                    <button class="btn btn-block btn-lg bg-pink waves-effect" type="submit">Registrarse</button>
                </form>
                <br />
                <div class="form-footer">
        <div class="row">
          <div class="col-xs-6 col-sm-6 col-md-6">
            <i class="fa fa-lock"></i>
            <a href="forget_password.php"> Olvidó su contraseña? </a>
          
          </div>
          
          <div class="col-xs-6 col-sm-6 col-md-6">
            <i class="fa fa-check"></i>
            <a href="index.php">  Iniciar sesión  </a>
          </div>
        </div>
      </div>
            </div>
        </div>
    </div>

    <!-- Jquery Core Js -->
    <script src="plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="plugins/node-waves/waves.js"></script>

    <!-- Validation Plugin Js -->
    <script src="plugins/jquery-validation/jquery.validate.js"></script>

    <!-- Custom Js -->
    <script src="js/admin.js"></script>
    <script src="js/pages/examples/sign-up.js"></script>
    <script src="js/register.js"></script>
</body>

</html>
﻿<?php require_once 'config.php'; ?>
<?php 
    if(!empty($_POST)){
        try {
            $user_obj = new Cl_User();
            $data = $user_obj->forgetPassword( $_POST );
            if($data)$success = PASSWORD_RESET_SUCCESS;
        } catch (Exception $e) {
            $error = $e->getMessage();
        }
    }
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Forgot Password | Bootstrap Based Admin Template - Material Design</title>
    <!-- Favicon-->
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="css/style.css" rel="stylesheet">
</head>

<body class="fp-page">
    <div class="fp-box">
        <div class="logo">
           <center><img src="images/logo_blanco.png" class="img-responsive text-center"></center>
        </div>
        <div class="card">
            <?php require_once 'templates/message.php';?>
            <div class="body">
                <form action="<?php echo $_SERVER['PHP_SELF']; ?>" id="forgetpassword-form" method="post"  class="form-register" role="form">
                    <div class="msg">
                        
<p>Ingrese su dirección de correo electrónico que utilizó para registrarse. Le enviaremos un correo electrónico con su password temporal, cambiala cuando ingreses a tu perfil</p>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">email</i>
                        </span>
                        <div class="form-line">
                            <input type="email" class="form-control" name="email" placeholder="Correo Electronico" required autofocus>
                            <span class="help-block"></span>
                        </div>
                    </div>

                    <button class="btn btn-block btn-lg bg-pink waves-effect" type="submit" data-loading-text="Restableciendo contraseña....">Restablecer Contraseña</button>

                    <div class="row m-t-20 m-b--5 align-center">
                        <a href="sign-in.html">Iniciar Sesion</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Jquery Core Js -->
    <script src="plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="plugins/node-waves/waves.js"></script>

    <!-- Validation Plugin Js -->
    <script src="plugins/jquery-validation/jquery.validate.js"></script>

    <!-- Custom Js -->
    <script src="js/admin.js"></script>

    <script src="js/jquery.validate.min.js"></script>
    <script src="js/forgetpassword.js"></script>
</body>

</html>
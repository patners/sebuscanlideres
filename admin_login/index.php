﻿<?php 
ob_start();
session_start();
require_once 'config.php'; 
?>
<?php 
    if( !empty( $_POST )){
        try {
            $user_obj = new Cl_User();
            $data = $user_obj->login( $_POST );
            if(isset($_SESSION['logged_in']) && $_SESSION['logged_in']){
                header('Location: home.php');
            }
        } catch (Exception $e) {
            $error = $e->getMessage();
        }
    }
    //print_r($_SESSION);
    if(isset($_SESSION['logged_in']) && $_SESSION['logged_in']){
        header('Location: home.php');
    }
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Iniciar sesion</title>
    <!-- Favicon-->
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="css/style.css" rel="stylesheet">
</head>

<body class="login-page">
    <div class="login-box">
        <div class="">
          <center><img src="images/logo_blanco.png" class="img-responsive text-center"></center>  
        </div>
        <div class="card">
            <?php require_once 'templates/message.php';?>
            <div class="body">
                <form id="login-form" method="post" class="form-signin" role="form" action="<?php echo $_SERVER['PHP_SELF']; ?>">
                    <div class="msg">iniciar sesion</div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                        <div class="form-line">
                            <input name="email" id="email" type="email" class="form-control" name="username" placeholder="correo" required autofocus>
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            <input name="password" id="password" type="password" class="form-control" name="password" placeholder="contraseña" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-4">
                            <button class="btn btn-block bg-pink waves-effect" type="submit" id="submit_btn" data-loading-text="Iniciando....">INICIAR</button>
                        </div>
                    </div>
                    <div class="row m-t-15 m-b--20">
                        <div class="col-xs-6">
                            <a href="registrar.php">Nuevo registro</a>
                        </div>
                        <div class="col-xs-6 align-right">
                            <a href="restablecer_password.php">Olvido la Contraseña?</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Jquery Core Js -->
    <script src="plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="plugins/node-waves/waves.js"></script>

    <!-- Validation Plugin Js -->
    <script src="plugins/jquery-validation/jquery.validate.js"></script>

    <!-- Custom Js -->
    <script src="js/admin.js"></script>
    <script src="js/login.js"></script>
    <script src="js/pages/examples/sign-in.js"></script>
</body>

</html>
<?php ob_end_flush(); ?>
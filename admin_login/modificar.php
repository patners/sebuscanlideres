<?php
	require 'conexion.php';
	//llamada a la base de datos validando el id
	$id = $_GET['id'];
	
	$sql = "SELECT * FROM tbl_login WHERE id = '$id'";
	$resultado = $mysqli->query($sql);
	$row = $resultado->fetch_array(MYSQLI_ASSOC);
	
?>
<html lang="es">
	<head>
		
		<meta name="viewport" content="width=device-width, initial-scale=1">
		 <link href="plugins/bootstrap/css/bootstrap.css" rel="stylesheet">
		<script src="js/jquery-3.1.1.min.js"></script>
		<script src="js/bootstrap.min.js"></script>	
	</head>
	
	<body>
		<div class="container">
			<div class="row">
				<h3 style="text-align:center">Usuario registrado</h3>
			</div>
			
			<form class="form-horizontal" method="POST" action="update.php" autocomplete="off">
				<div class="form-group">
					<label for="nombre" class="col-sm-2 control-label">Nombres</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="nplataforma" name="nplataforma" placeholder="Nombre" value="<?php echo $row['first_name']; ?>" required>
					</div>
				</div>
			
				
				<div class="form-group">
					<label for="text" class="col-sm-2 control-label">Apellido paterno</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="tusu" name="tusu" placeholder="" value="<?php echo $row['last_name']; ?>"  required>
					</div>
				</div>
				
				<div class="form-group">
					<label for="" class="col-sm-2 control-label">Usuario</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="usu" name="usu" placeholder="" value="<?php echo $row['usuario']; ?>" >
					</div>
				</div>
                
                <div class="form-group">
					<label for="" class="col-sm-2 control-label">Contraseña</label>
					<div class="col-sm-10">
						<input type="password" class="form-control" id="pass" name="pass" placeholder="" value="<?php echo $row['contraseña']; ?>" >
					</div>
				</div>
                
                <div class="form-group">
					<label for="telefono" class="col-sm-2 control-label">URL</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="turl" name="turl" placeholder="" value="<?php echo $row['URL']; ?>" >
					</div>
				</div>


				
				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
						<a href="index.php" class="btn btn-default">Regresar</a>
					</div>
				</div>
			</form>
		</div>
	</body>
</html>
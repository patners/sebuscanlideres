<?php
$mysqli = new mysqli('localhost', 'root', 'amorlibre23', 'liberity');
?>
<?php

$connect = new PDO("mysql:host=localhost;dbname=liberity", "root", "amorlibre23");
$message = '';
if(isset($_POST["email"]))
{
 sleep(5);
 $query = "
 INSERT INTO tbl_login 
 (first_name, last_name, last_name1, fecha, gender, estado,mobile_no,email, estudia, donde_estudia, carrera, lider, respuesta_lider, influyente, respuesta_influyente, problematica, queja_estado, gobiernos, movilidad, migracion,economia, democracia, estadoderecho, medio_ambiente, seguridad,sociedad) VALUES 
 (:first_name, :last_name, :last_name1, :fecha , :gender, :estado, :mobile_no,:email, :estudia, :donde_estudia, :carrera, :lider, :respuesta_lider, :influyente, :respuesta_influyente, :problematica, :queja_estado, :gobiernos, :movilidad, :migracion, :economia, :democracia, :estadoderecho, :medio_ambiente, :seguridad, :sociedad)
 ";

 $user_data = array(
  ':first_name'  => $_POST["first_name"],
  ':last_name'  => $_POST["last_name"],
  ':last_name1'  => $_POST["last_name1"],
  ':fecha'  => $_POST["fecha"],
  ':gender'   => $_POST["gender"],
  ':estado'  => $_POST["estado"],
  ':mobile_no'  => $_POST["mobile_no"],
  ':email'   => $_POST["email"],
  ':estudia'   => $_POST["estudia"],
  ':donde_estudia'   => $_POST["donde_estudia"],
  ':carrera'   => $_POST["carrera"],
  ':lider'   => $_POST["lider"],
  ':respuesta_lider'   => $_POST["respuesta_lider"],
  ':influyente'   => $_POST["influyente"],
  ':respuesta_influyente'   => $_POST["respuesta_influyente"],
  ':problematica'   => $_POST["problematica"],
  ':queja_estado'   => $_POST["queja_estado"],
  ':gobiernos'   => $_POST["gobiernos"],
  ':movilidad'   => $_POST["movilidad"],
  ':migracion'   => $_POST["migracion"],
  ':economia'   => $_POST["economia"],
  ':democracia'   => $_POST["democracia"],
  ':estadoderecho'   => $_POST["estadoderecho"],
  ':seguridad'   => $_POST["seguridad"],
  ':sociedad'   => $_POST["sociedad"],
  ':medio_ambiente'   => $_POST["medio_ambiente"]
 );
 $statement = $connect->prepare($query);
 if($statement->execute($user_data))
 {
  $message = '
  <div class="alert alert-success">
  Registro Completado
  </div>
  ';
 }
 else
 {
  $message = '
  <div class="alert alert-success">
  Error al registrarte
  </div>
  ';
 }
}
?>
<!DOCTYPE html>
<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>REGISTRO SE BUSCAN LIDERES</title>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
 <link href="plugins/bootstrap/css/bootstrap.css" rel="stylesheet">
 <script src="plugins/bootstrap/js/bootstrap.js"></script>
  <style>
    body{
      background-image: url(./images/fondo_formulario.png);
  background-size: cover;
  background-position: center center;
  background-repeat: no-repeat;
   min-height: 100vh;
    }
   
  .box
  {
   margin:0 auto;
  }
  .active_tab1
  {
   background-color:#fff;
   color:#333;
   font-weight: 600;
  }
  .inactive_tab1
  {
   background-color: #f5f5f5;
   color: #333;
   cursor: not-allowed;
  }
  .has-error
  {
   border-color:#cc0000;
   background-color:#ffff99;
  }
  .nav-tabs {
    border-bottom: none;
}
.image{
  width: 120px;
  height: 100%;
}
  </style>
 </head>
 <body>
 <nav class="navbar navbar-inverse">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <div class="image">
                     <center><img src="images/logo_blanco.png" class="img-responsive text-center"></center>  
                </div>
          </div>
        </div><!--/.container-fluid -->
      </nav>

  <div class="container box card">
<br />
   <?php echo $message; ?>
   <form method="post" id="register_form" class="needs-validation">
    <ul class="nav nav-tabs">
     <li class="nav-item">
      <a class="nav-link active_tab1" style="border:1px solid #ccc" id="list_login_details">DATOS GENERALES</a>
     </li>
     <li class="nav-item">
      <a class="nav-link inactive_tab1" id="list_personal_details" style="border:1px solid #ccc">PERFIL</a>
     </li>
     <li class="nav-item">
      <a class="nav-link inactive_tab1" id="list_contact_details" style="border:1px solid #ccc">PERFIL</a>
     </li>
    </ul>
    <div class="tab-content" style="margin-top:8px;">
     <div class="tab-pane active" id="login_details">
      <div class="panel panel-default">
       <div class="panel-body">
         <br />
         <div class="form-group col-md-4 col-sm-12">
         <label>Nombre</label>
         <input type="text" name="first_name" id="first_name" class="form-control" required/>
         <span id="error_first_name" class="text-danger"></span>
        </div>
         <div class="form-group col-md-4 col-sm-12">
         <label>Apellido paterno</label>
         <input type="text" name="last_name" id="last_name" class="form-control" required />
         <span id="error_last_name" class="text-danger"></span>
        </div>
        <div class="form-group col-md-4 col-sm-12">
         <label>Apellido Materno</label>
         <input type="text" name="last_name1" id="last_name1" required class="form-control" />
         <span id="error_last_name" class="text-danger"></span>
        </div>
        <div class="form-group col-md-6 col-sm-12">
         <label>Fecha Nacimiento</label>
         <input type="date" name="fecha" id="fecha" class="form-control" />
         <span id="error_fecha" class="text-danger"></span>
        </div>

        <div class="form-group col-md-6 col-sm-12">
         <label>Estado de la Republica</label>
          <select class="form-control" id="estado" name="estado" required>
              <option></option>
              <option>Aguascalientes</option>
              <option>Baja California</option>
              <option>Baja California Sur</option>
              <option>Campeche</option>
              <option>Coahuila de Zaragoza</option>
              <option>Colima</option>
              <option>Chiapas</option>
              <option>Chihuahua</option>
              <option>CDMX</option>
              <option>Durango</option>
              <option>Guanajuato</option>
              <option>Guerrero</option>
              <option>Hidalgo</option>
              <option>Jalisco</option>
              <option>México</option>
              <option>Michoacán de Ocampo</option>
              <option>Morelos</option>
              <option>Nayarit</option>
              <option>Nuevo León</option>
              <option>Oaxaca</option>
              <option>Puebla</option>
              <option>Querétaro</option>
              <option>Quintana Roo</option>
              <option>San Luis Potosí</option>
              <option>Sinaloa</option>
              <option>Sonora</option>
              <option>Tabasco</option>
              <option>Tamaulipas</option>
              <option>Tlaxcala</option>
              <option>Veracruz</option>
              <option>Yucatán</option>
              <option>Zacatecas</option>
          </select>
         <span id="error_estado" class="text-danger"></span>
        </div>
         <div class="form-group col-md-6 col-sm-12">
         <label>Teléfono</label>
         <input type="number" max="10" name="mobile_no" id="mobile_no" class="form-control" />
         <span id="error_mobile_no" class="text-danger"></span>
        </div>
        <div class="form-group col-md-6 col-sm-12">
         <label>Correo Electronico</label>
         <input type="text" name="email" id="email" class="form-control" />
         <span id="error_email" class="text-danger"></span>
        </div>
        <div class="form-group col-md-12 col-sm-12">
         <label>Genero</label>
         <label class="radio-inline">
          <input type="radio" name="gender" value="hombre" checked> Hombre
         </label>
         <label class="radio-inline">
          <input type="radio" name="gender" value="mujer"> Mujer
         </label>
         <label class="radio-inline">
          <input type="radio" name="gender" value="Prefiero no decirlo"> Prefiero no decirlo
         </label>
        </div>
        <br />
        <div align="center">
         <button type="button" name="btn_login_details" id="btn_login_details" class="btn btn-info btn-lg">Next</button>
        </div>
        <br />
       </div>
      </div>
     </div>
     <div class="tab-pane fade" id="personal_details">
      <div class="panel panel-default">
       <div class="panel-body">
  
        <div class="form-group col-md-4 col-sm-12">
         <label>¿Estudias Actualmente?</label>
         <br />
         <select class="form-control" id="estudia" name="estudia" required>
              <option> </option>
              <option>Si estoy estudiando</option>
              <option>Estoy trabajando</option>
              <option>Estudio y Trabajo</option>
            </select>
        </div>
        <div class="form-group col-md-8 col-sm-12">
         <label>¿En qué Universidad estudias o estudiaste?</label>
         <input type="text" name="donde_estudia" id="donde_estudia" class="form-control" />
         <span id="error_donde_estudia" class="text-danger"></span>
        </div>
        <div class="form-group col-md-12 col-sm-12">
         <label>¿Qué carrera estás estudiando/estudiaste?</label>
         <input type="text" name="carrera" id="carrera" class="form-control" />
         <span id="error_carrera" class="text-danger"></span>
        </div>
        <div class="form-group col-md-4 col-sm-12">
         <label>¿Te consideras un líder?</label>
         <br />
         <label class="radio-inline">
          <input type="radio" name="lider" value="soylider" checked> SI
         </label>
         <label class="radio-inline">
          <input type="radio" name="lider" value="no"> NO
         </label>
        </div>
        <div class="form-group col-md-8">
         <label>De contestar afirmativamente a la pregunta anterior ¿por qué?</label>
         <textarea name="respuesta_lider" id="respuesta_lider" class="form-control"></textarea>
         <span id="error_respuesta_lider" class="text-danger"></span>
        </div>
        <div class="form-group col-md-4 col-sm-12">
         <label>¿Consideras que influyes en tu comunidad? </label>
         <br />
         <label class="radio-inline">
          <input type="radio" name="influyente" value="si" checked> SI
         </label>
         <label class="radio-inline">
          <input type="radio" name="influyente" value="no"> NO
         </label>
        </div>
        <div class="form-group col-md-8">
         <label>De contestar afirmativamente a la pregunta anterior ¿Cómo influyes en tu comunidad?</label>
         <textarea name="respuesta_influyente" id="respuesta_influyente" class="form-control"></textarea>
         <span id="error_respuesta_influyente" class="text-danger"></span>
        </div>
        <div class="form-group col-md-12">
         <label>¿Cuál consideras qué es la problemática de mayor importancia que aqueja al país?</label>
         <textarea name="problematica" id="problematica" class="form-control"></textarea>
         <span id="error_probelmatica" class="text-danger"></span>
        </div>
        <div class="form-group col-md-12">
         <label>¿Cuál consideras que es la problemática de mayor importancia que aqueja tu estado?</label>
         <textarea name="queja_estado" id="queja_estado" class="form-control"></textarea>
         <span id="error_queja_estado" class="text-danger"></span>
        </div>
        <br />
        <div align="center">
         <button type="button" name="previous_btn_personal_details" id="previous_btn_personal_details" class="btn btn-default btn-lg">Previous</button>
         <button type="button" name="btn_personal_details" id="btn_personal_details" class="btn btn-info btn-lg">Next</button>
        </div>
        <br />
       </div>
      </div>
     </div>
     <div class="tab-pane fade" id="contact_details">
      <div class="panel panel-default">
       <div class="panel-body">
        <div class="form-group col-md-12">
         <label>De los siguientes temas,ordénalos de acuerdo a tus intereses llenandolo con numero del 1 al 9</label>
        </div>
        <div class="form-group col-md-4">
          <label>Gobiernos digitales</label>
         <select class="form-control" id="gobiernos" name="gobiernos">
          <option>selecciona numero</option>
            <option>1</option>
            <option>2</option>
            <option>3</option>
            <option>4</option>
            <option>5</option>
            <option>6</option>
            <option>7</option>
            <option>8</option>
            <option>9</option>
          </select>
         <span id="error_estado" class="text-danger"></span>
        </div>
        <div class="form-group col-md-4">
          <label>Movilidad</label>
         <select class="form-control" id="movilidad" name="movilidad">
          <option>selecciona numero</option>
            <option>1</option>
            <option>2</option>
            <option>3</option>
            <option>4</option>
            <option>5</option>
            <option>6</option>
            <option>7</option>
            <option>8</option>
            <option>9</option>
          </select>
         <span id="error_estado" class="text-danger"></span>
        </div>
        <div class="form-group col-md-4">
          <label>Migración</label>
         <select class="form-control" id="migracion" name="migracion">
          <option>selecciona numero</option>
            <option>1</option>
            <option>2</option>
            <option>3</option>
            <option>4</option>
            <option>5</option>
            <option>6</option>
            <option>7</option>
            <option>8</option>
            <option>9</option>
          </select>
         <span id="error_estado" class="text-danger"></span>
        </div>
        <div class="form-group col-md-4">
          <label>Economía</label>
         <select class="form-control" id="economia" name="economia">
          <option>selecciona numero</option>
            <option>1</option>
            <option>2</option>
            <option>3</option>
            <option>4</option>
            <option>5</option>
            <option>6</option>
            <option>7</option>
            <option>8</option>
            <option>9</option>
          </select>
         <span id="error_estado" class="text-danger"></span>
        </div>
        <div class="form-group col-md-4">
          <label>Democracia</label>
         <select class="form-control" id="democracia" name="democracia">
          <option>selecciona numero</option>
            <option>1</option>
            <option>2</option>
            <option>3</option>
            <option>4</option>
            <option>5</option>
            <option>6</option>
            <option>7</option>
            <option>8</option>
            <option>9</option>
          </select>
         <span id="error_estado" class="text-danger"></span>
        </div>
        <div class="form-group col-md-4">
          <label>Estado de derecho</label>
         <select class="form-control" id="estadoderecho" name="estadoderecho">
            <option>selecciona numero</option>
            <option>1</option>
            <option>2</option>
            <option>3</option>
            <option>4</option>
            <option>5</option>
            <option>6</option>
            <option>7</option>
            <option>8</option>
            <option>9</option>
          </select>
         <span id="error_estado" class="text-danger"></span>
        </div>
        <div class="form-group col-md-4">
          <label>Medio Ambiente y desarrollo sustentable</label>
         <select class="form-control" id="medio_ambiente" name="medio_ambiente">
            <option>selecciona numero</option>
            <option>1</option>
            <option>2</option>
            <option>3</option>
            <option>4</option>
            <option>5</option>
            <option>6</option>
            <option>7</option>
            <option>8</option>
            <option>9</option>
          </select>
         <span id="error_estado" class="text-danger"></span>
        </div>
        <div class="form-group col-md-4">
          <label>Seguridad y políticas públicas contra el crimen</label>
         <select class="form-control" id="seguridad" name="seguridad">
            <option>selecciona numero</option>
            <option>1</option>
            <option>2</option>
            <option>3</option>
            <option>4</option>
            <option>5</option>
            <option>6</option>
            <option>7</option>
            <option>8</option>
            <option>9</option>
          </select>
         <span id="error_estado" class="text-danger"></span>
        </div>
        <div class="form-group col-md-4">
          <label>Sociedad civil</label>
          <br/>
         <select class="form-control" id="sociedad" name="sociedad">
            <option>selecciona numero</option>
            <option>1</option>
            <option>2</option>
            <option>3</option>
            <option>4</option>
            <option>5</option>
            <option>6</option>
            <option>7</option>
            <option>8</option>
            <option>9</option>
          </select>
         <span id="error_estado" class="text-danger"></span>
        </div>
        <br />
        <div class="form-group col-md-12">
         <label>¿Has asistido a algún taller/capacitación/programa de liderazgo?</label>
         <textarea name="taller" id="taller" class="form-control"></textarea>
         <span id="error_taller" class="text-danger"></span>
        </div>
        <div class="form-group col-md-12">
         <label>¿Qué habilidades consideras debes trabajar para mejorar tu liderazgo?</label>
         <textarea name="habilidades" id="habulidades" class="form-control"></textarea>
         <span id="error_habilidades" class="text-danger"></span>
        </div>
        <div class="form-group col-md-12">
         <label>Me gustaría formar parte del programa #SeBuscanLíderes porque…</label>
         <textarea name="sebuscanlideres" id="sebuscanlideres" class="form-control"></textarea>
         <span id="error_sebuscanlideres" class="text-danger"></span>
        </div>

        <br />
        <div align="form-group center">
         <button type="button" name="previous_btn_contact_details" id="previous_btn_contact_details" class="btn btn-default btn-lg">Previous</button>
         <button type="button" name="btn_contact_details" id="btn_contact_details" class="btn btn-success btn-lg">Register</button>
        </div>
        <br />
       </div>
      </div>
     </div>
    </div>
   </form>
  </div>
 </body>
</html>

<script>
$(document).ready(function(){
 
 $('#btn_login_details').click(function(){
  var error_first_name = '';
  var error_last_name = '';
  var error_fecha = '';
  if($.trim($('#first_name').val()).length == 0)
  {
   error_first_name = 'Ingresa tu nombre';
   $('#error_first_name').text(error_first_name);
   $('#first_name').addClass('has-error');
  }
  else
  {
   error_first_name = '';
   $('#error_first_name').text(error_first_name);
   $('#first_name').removeClass('has-error');
  }

  if($.trim($('#last_name').val()).length == 0)
  {
   error_last_name = 'Ingresa tu Apellido Paterno';
   $('#error_last_name').text(error_last_name);
   $('#last_name').addClass('has-error');
  }
  else
  {
   error_last_name = '';
   $('#error_last_name').text(error_last_name);
   $('#last_name').removeClass('has-error');
  }

   if($.trim($('#fecha').val()).length == 0)
  {
   error_fecha = 'Ingresa tu Fecha de Nacimiento';
   $('#error_fecha').text(error_fecha);
   $('#fecha').addClass('has-error');
  }
  else
  {
   error_fecha = '';
   $('#error_fecha').text(error_fecha);
   $('#fecha').removeClass('has-error');
  }

   if($.trim($('#estado').val()).length == 0)
  {
   error_estado = 'Selecciona un Estado';
   $('#error_estado').text(error_estado);
   $('#estado').addClass('has-error');
  }
  else
  {
   error_estado = '';
   $('#error_estado').text(error_estado);
   $('#estado').removeClass('has-error');
  }

  /*Validacion telefono*/
   if($.trim($('#mobile_no').val()).length == 0)
  {
   error_mobile_no = 'Ingresa tu numero telefonico';
   $('#error_mobile_no').text(error_mobile_no);
   $('#mobile_no').addClass('has-error');
  }
  else
  {
   error_mobile_no = '';
   $('#error_email').text(error_mobile_no);
   $('#mobile_no').removeClass('has-error');
  }
  /*fin validacion*/

  /*Validacion correo*/
   if($.trim($('#email').val()).length == 0)
  {
   error_email = 'Ingresa tu correo';
   $('#error_email').text(error_email);
   $('#email').addClass('has-error');
  }
  else
  {
   error_email = '';
   $('#error_email').text(error_email);
   $('#email').removeClass('has-error');
  }
  /*fin validacion*/

  /*parametros de validacion*/
  if(error_first_name != '' || error_last_name != '' || error_fecha != '' || error_estado != '' || error_mobile_no != '' || error_email != '')
  {
   return false;
  }
 
  else
  {
   $('#list_login_details').removeClass('active active_tab1');
   $('#list_login_details').removeAttr('href data-toggle');
   $('#login_details').removeClass('active');
   $('#list_login_details').addClass('inactive_tab1');
   $('#list_personal_details').removeClass('inactive_tab1');
   $('#list_personal_details').addClass('active_tab1 active');
   $('#list_personal_details').attr('href', '#personal_details');
   $('#list_personal_details').attr('data-toggle', 'tab');
   $('#personal_details').addClass('active in');
  }
 });
 
 $('#previous_btn_personal_details').click(function(){
  $('#list_personal_details').removeClass('active active_tab1');
  $('#list_personal_details').removeAttr('href data-toggle');
  $('#personal_details').removeClass('active in');
  $('#list_personal_details').addClass('inactive_tab1');
  $('#list_login_details').removeClass('inactive_tab1');
  $('#list_login_details').addClass('active_tab1 active');
  $('#list_login_details').attr('href', '#login_details');
  $('#list_login_details').attr('data-toggle', 'tab');
  $('#login_details').addClass('active in');
 });
 
 $('#btn_personal_details').click(function(){

   $('#list_personal_details').removeClass('active active_tab1');
   $('#list_personal_details').removeAttr('href data-toggle');
   $('#personal_details').removeClass('active');
   $('#list_personal_details').addClass('inactive_tab1');
   $('#list_contact_details').removeClass('inactive_tab1');
   $('#list_contact_details').addClass('active_tab1 active');
   $('#list_contact_details').attr('href', '#contact_details');
   $('#list_contact_details').attr('data-toggle', 'tab');
   $('#contact_details').addClass('active in');
  
 });
 
 $('#previous_btn_contact_details').click(function(){
  $('#list_contact_details').removeClass('active active_tab1');
  $('#list_contact_details').removeAttr('href data-toggle');
  $('#contact_details').removeClass('active in');
  $('#list_contact_details').addClass('inactive_tab1');
  $('#list_personal_details').removeClass('inactive_tab1');
  $('#list_personal_details').addClass('active_tab1 active');
  $('#list_personal_details').attr('href', '#personal_details');
  $('#list_personal_details').attr('data-toggle', 'tab');
  $('#personal_details').addClass('active in');
 });
 
 $('#btn_contact_details').click(function(){
  var sebuscanlideres = '';

  if($.trim($('#sebuscanlideres').val()).length == 0)
  {
   sebuscanlideres = 'campo is required';
   $('#sebuscanlideres').text(sebuscanlideres);
   $('#address').addClass('has-error');
  }
  else
  {
   sebuscanlideres = '';
   $('#sebuscanlideres').text(sebuscanlideres);
   $('#address').removeClass('has-error');
  }
  
  if(sebuscanlideres != '')
  {
   return false;
  }
  else
  {
   $('#btn_contact_details').attr("disabled", "disabled");
   $(document).css('cursor', 'prgress');
   $("#register_form").submit();
  }
  
 });
 
});
</script>